package com.authenticationservice.authenticationservice.IntegrationTests;

import com.authenticationservice.authenticationservice.domain.ConfirmationDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenRepositoryStub extends CrudRepository<ConfirmationDto, String> {

    public abstract List<ConfirmationDto> findAll();
}
