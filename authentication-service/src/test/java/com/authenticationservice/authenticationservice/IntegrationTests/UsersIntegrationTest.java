package com.authenticationservice.authenticationservice.IntegrationTests;

import com.authenticationservice.authenticationservice.domain.ConfirmationDto;
import com.authenticationservice.authenticationservice.domain.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles({"test"})
public class UsersIntegrationTest {
    TestRestTemplate restTemplate = new TestRestTemplate();

    @LocalServerPort
    int port;

    @Autowired
    TokenRepositoryStub tokenRepository;

    private String getURI(String uri) {
        return "http://localhost:" + port + uri;
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Accept", "application/json");
        httpHeaders.add("Content-Type", "application/json");
        return httpHeaders;
    }

    @Test
    public void shouldRegisterUser() {
        Users users = new Users();
        users.setEmailId("example@example.com");
        users.setPassword("examplePassword");
        HttpEntity<Users> entity = new HttpEntity<>(users, getHttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate.exchange(getURI("/register"), HttpMethod.POST, entity, String.class);
        assertEquals(200, responseEntity.getStatusCode().value());
        assertEquals("Created", responseEntity.getBody());
    }

    @Test
    public void shouldNotRegisterUserWhenEmailExists() {
        shouldRegisterUser();
        Users users = new Users();
        users.setEmailId("example@example.com");
        users.setPassword("examplePassword");
        HttpEntity<Users> entity = new HttpEntity<>(users, getHttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate.exchange(getURI("/register"), HttpMethod.POST, entity, String.class);
        assertEquals(400, responseEntity.getStatusCode().value());
        assertEquals("User already Exists for this email", responseEntity.getBody());
    }

    @Test
    public void shouldVerifyUserPostRegistration() {
        shouldRegisterUser();
        List<ConfirmationDto> confirmationDtos = tokenRepository.findAll();
        HttpEntity<Users> entity = new HttpEntity<>(null, getHttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate.exchange(getURI("/verify?token=" + confirmationDtos.get(0).getConfirmationToken()), HttpMethod.GET, entity, String.class);
        assertEquals(200, responseEntity.getStatusCode().value());
        assertEquals("User Verified Successfully!", responseEntity.getBody());
    }

    @Test
    public void shouldNotVerifyUserIfInvalidTokenIsPassed() {
        HttpEntity<Users> entity = new HttpEntity<>(null, getHttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate.exchange(getURI("/verify?token=dfgd"), HttpMethod.GET, entity, String.class);
        assertEquals(400, responseEntity.getStatusCode().value());
        assertEquals("Verification Failed!", responseEntity.getBody());
    }

    @Test
    public void shouldNotLoginInvalidUserPostVerification() {
        shouldVerifyUserPostRegistration();
        Users users = new Users();
        users.setEmailId("example@example.com");
        users.setPassword("examplePassword3");
        HttpEntity<Users> entity = new HttpEntity<>(users, getHttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate.exchange(getURI("/login"), HttpMethod.POST, entity, String.class);
        assertEquals(400, responseEntity.getStatusCode().value());
        assertEquals("Invalid Password!", responseEntity.getBody());
    }

    @Test
    public void shouldLoginUserPostVerification() {
        shouldVerifyUserPostRegistration();
        Users users = new Users();
        users.setEmailId("example@example.com");
        users.setPassword("examplePassword");
        HttpEntity<Users> entity = new HttpEntity<>(users, getHttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate.exchange(getURI("/login"), HttpMethod.POST, entity, String.class);
        assertEquals(200, responseEntity.getStatusCode().value());
        assertNotNull(responseEntity.getBody());
    }

    @Test
    public void shouldNotLoginUnVerifiedUser() {
        shouldRegisterUser();
        Users users = new Users();
        users.setEmailId("example@example.com");
        users.setPassword("examplePassword");
        HttpEntity<Users> entity = new HttpEntity<>(users, getHttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate.exchange(getURI("/login"), HttpMethod.POST, entity, String.class);
        assertEquals(400, responseEntity.getStatusCode().value());
        assertEquals("User is Not Verified!", responseEntity.getBody());
    }
    @Test
    public void shouldNotLoginUnRegisteredUser() {
        Users users = new Users();
        users.setEmailId("example@example.com");
        users.setPassword("examplePassword");
        HttpEntity<Users> entity = new HttpEntity<>(users, getHttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate.exchange(getURI("/login"), HttpMethod.POST, entity, String.class);
        assertEquals(400, responseEntity.getStatusCode().value());
        assertEquals("User Not Exists!", responseEntity.getBody());
    }
}
