package com.authenticationservice.authenticationservice.UnitTests.service;

import com.authenticationservice.authenticationservice.domain.UsersDto;
import com.authenticationservice.authenticationservice.repository.UserRepository;
import com.authenticationservice.authenticationservice.service.UserDetailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailServiceTest {

    @InjectMocks
    private UserDetailService userDetailService;

    @Mock
    private UserRepository userDao;

    @Test
    public void shouldReturnUserByEmailId() {
        String email = "example5@example.com";
        String password = "encryptedPassword";
        UsersDto user = new UsersDto();
        user.setUserid(1L);
        user.setEmailId(email);
        user.setPassword(password);
        when(userDao.findByEmailIdIgnoreCase(email)).thenReturn(user);
        UserDetails userDetails = userDetailService.loadUserByUsername(email);
        assertEquals(email, userDetails.getUsername());
        assertEquals(password, userDetails.getPassword());
    }
}
