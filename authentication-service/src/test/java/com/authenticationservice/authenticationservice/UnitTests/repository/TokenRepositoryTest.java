package com.authenticationservice.authenticationservice.UnitTests.repository;

import com.authenticationservice.authenticationservice.domain.ConfirmationDto;
import com.authenticationservice.authenticationservice.domain.Role;
import com.authenticationservice.authenticationservice.domain.Users;
import com.authenticationservice.authenticationservice.domain.UsersDto;
import com.authenticationservice.authenticationservice.repository.TokenRepository;
import com.authenticationservice.authenticationservice.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles({"test"})
public class TokenRepositoryTest {

    @Autowired
    private UserRepository usersRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Test
    public void shouldSaveAndReturnToken() {
        String email = "example@gmail.com";
        Users user = new Users();
        user.setEmailId(email);
        user.setIsEnabled(0);
        user.setFirstName("first");
        user.setLastName("last");
        user.setPassword("password");
        user.setRole(Role.USER);
        UsersDto usersDto = new UsersDto(user);
        usersRepository.save(usersDto);
        ConfirmationDto confirmation = new ConfirmationDto();
        confirmation.setConfirmationToken("tokenData");
        confirmation.setCreatedDate(new Date());
        confirmation.setUser(usersDto);
        tokenRepository.save(confirmation);
        ConfirmationDto tokenInfo = tokenRepository.findByConfirmationToken("tokenData");
        assertEquals(confirmation.getConfirmationToken(), tokenInfo.getConfirmationToken());
        assertEquals(confirmation.getCreatedDate(), tokenInfo.getCreatedDate());
        assertEquals(confirmation.getTokenid(), tokenInfo.getTokenid());
        assertEquals(confirmation.getUser().getUserid(), tokenInfo.getUser().getUserid());
    }


}