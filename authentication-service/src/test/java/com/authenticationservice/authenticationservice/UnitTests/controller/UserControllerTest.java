package com.authenticationservice.authenticationservice.UnitTests.controller;

import com.authenticationservice.authenticationservice.config.MessagePublisher;
import com.authenticationservice.authenticationservice.config.TokenUtil;
import com.authenticationservice.authenticationservice.controller.UserController;
import com.authenticationservice.authenticationservice.domain.ConfirmationDto;
import com.authenticationservice.authenticationservice.domain.UsersDto;
import com.authenticationservice.authenticationservice.repository.TokenRepository;
import com.authenticationservice.authenticationservice.repository.UserRepository;
import net.minidev.json.JSONObject;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
public class UserControllerTest {
    private MockMvc mockMvc;

    @InjectMocks
    private UserController userController;
    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder bcryptEncoder;
    @Mock
    private MessagePublisher messagePublisher;
    @Mock
    private UserDetailsService userDetailsService;
    @Mock
    private TokenRepository tokenRepository;
    @Mock
    private TokenUtil tokenUtil;


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .build();
    }


    @Test
    public void shouldRegisterUser() throws Exception {
        String email = "example5@example.com";
        String password = "1234354";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("emailId", email);
        jsonObject.put("password", password);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonObject.toString());    when(userRepository.findByEmailIdIgnoreCase(email)).thenReturn(null);
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("Created")));
    }

    @Test
    public void shouldNotRegisterUserWhenEmailExists() throws Exception {
        String email = "example5@example.com";
        String password = "1234354";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("emailId", email);
        jsonObject.put("password", password);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonObject.toString());  when(userRepository.findByEmailIdIgnoreCase(email)).thenReturn(new UsersDto());
        mockMvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("User already Exists for this email")));
    }

    @Test
    public void shouldVerifyUserPostRegistration() throws Exception {
        String email = "example5@example.com";
        String token = "token12";
        UsersDto user = new UsersDto();
        user.setUserid(1L);
        user.setEmailId(email);
        ConfirmationDto confirmationDto = new ConfirmationDto();
        confirmationDto.setTokenid(1L);
        confirmationDto.setConfirmationToken(token);
        confirmationDto.setUser(user);
        when(tokenRepository.findByConfirmationToken(token)).thenReturn(confirmationDto);

        when(userRepository.findByEmailIdIgnoreCase(email)).thenReturn(user);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/verify?token=" + token)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("User Verified Successfully!")));

    }

    @Test
    public void shouldNotVerifyUserIfInvalidTokenIsPassed() throws Exception {
        String token = "token12";
        when(tokenRepository.findByConfirmationToken(token)).thenReturn(null);
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/verify?token=" + token)
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("Verification Failed!")));
    }

    @Test
    public void shouldNotLoginInvalidUserPostVerification() throws Exception {
        String email = "example5@example.com";
        String password = "1234354";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("emailId", email);
        jsonObject.put("password", password);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonObject.toString());
        UsersDto user = new UsersDto();
        user.setUserid(1L);
        user.setEmailId(email);
        user.setIsEnabled(1);
        user.setPassword("encryptedPassword");
        when(userRepository.findByEmailIdIgnoreCase(email)).thenReturn(user);
        when(bcryptEncoder.matches(password, user.getPassword())).thenReturn(false);

        mockMvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("Invalid Password!")));
    }

    @Test
    public void shouldLoginUserPostVerification() throws Exception {
        String email = "example5@example.com";
        String password = "1234354";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("emailId", email);
        jsonObject.put("password", password);
        UsersDto user = new UsersDto();
        user.setUserid(1L);
        user.setEmailId(email);
        user.setIsEnabled(1);
        user.setPassword("encryptedPassword");
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonObject.toString());

        when(userRepository.findByEmailIdIgnoreCase(email)).thenReturn(user);
        when(bcryptEncoder.matches(password, user.getPassword())).thenReturn(true);
        mockMvc.perform(request).andExpect(status().isOk());
    }


    @Test
    public void shouldNotLoginUnVerifiedUser() throws Exception {
        String email = "example5@example.com";
        String password = "1234354";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("emailId", email);
        jsonObject.put("password", password);
        UsersDto user = new UsersDto();
        user.setUserid(1L);
        user.setEmailId(email);
        user.setPassword("encryptedPassword");
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonObject.toString());

        when(userRepository.findByEmailIdIgnoreCase(email)).thenReturn(user);
        mockMvc.perform(request).andExpect(status().isBadRequest()).andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("User is Not Verified!")));
    }

    @Test
    public void shouldNotLoginUnRegisteredUser() throws Exception {
        String email = "example5@example.com";
        String password = "1234354";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("emailId", email);
        jsonObject.put("password", password);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonObject.toString());

        when(userRepository.findByEmailIdIgnoreCase(email)).thenReturn(null);
        mockMvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("User Not Exists!")));
    }

}
