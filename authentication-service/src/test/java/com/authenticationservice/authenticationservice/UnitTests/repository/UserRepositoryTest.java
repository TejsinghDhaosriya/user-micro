package com.authenticationservice.authenticationservice.UnitTests.repository;

import com.authenticationservice.authenticationservice.domain.Role;
import com.authenticationservice.authenticationservice.domain.Users;
import com.authenticationservice.authenticationservice.domain.UsersDto;
import com.authenticationservice.authenticationservice.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles({"test"})
public class UserRepositoryTest {

    @Autowired
    private UserRepository usersRepository;

    @Test
    public void shouldSaveAndReturnUserWhenRoleIsUser() {
        String email ="example@gmail.com";
        Users user  = new Users();
        user.setEmailId(email);
        user.setIsEnabled(0);
        user.setFirstName("first");
        user.setLastName("last");
        user.setPassword("password");
        user.setRole(Role.USER);
        usersRepository.save(new UsersDto(user));
        UsersDto usersDto= usersRepository.findByEmailIdIgnoreCase(email);
        assertEquals(user.getFirstName(), usersDto.getFirstName());
        assertEquals(user.getLastName(), usersDto.getLastName());
        assertEquals(user.getEmailId(), usersDto.getEmailId());
        assertEquals(user.getPassword(), usersDto.getPassword());
        assertEquals(user.getIsEnabled(), usersDto.getIsEnabled());
        assertEquals(user.getRole(),usersDto.getRole());
    }
    @Test
    public void shouldSaveAndReturnUserWhenRoleIsAdmin() {
        String email ="example@gmail.com";
        Users user  = new Users();
        user.setEmailId(email);
        user.setIsEnabled(0);
        user.setFirstName("first");
        user.setLastName("last");
        user.setPassword("password");
        user.setRole(Role.ADMIN);
        usersRepository.save(new UsersDto(user));
        UsersDto usersDto= usersRepository.findByEmailIdIgnoreCase(email);
        assertEquals(user.getFirstName(), usersDto.getFirstName());
        assertEquals(user.getLastName(), usersDto.getLastName());
        assertEquals(user.getEmailId(), usersDto.getEmailId());
        assertEquals(user.getPassword(), usersDto.getPassword());
        assertEquals(user.getIsEnabled(), usersDto.getIsEnabled());
        assertEquals(user.getRole(),usersDto.getRole());
    }
    @Test
    public void shouldSaveAndReturnUserWhenRoleIsNotPassed() {
        String email ="example@gmail.com";
        Users user  = new Users();
        user.setEmailId(email);
        user.setIsEnabled(0);
        user.setFirstName("first");
        user.setLastName("last");
        user.setPassword("password");
        usersRepository.save(new UsersDto(user));
        UsersDto usersDto= usersRepository.findByEmailIdIgnoreCase(email);
        assertEquals(user.getFirstName(), usersDto.getFirstName());
        assertEquals(user.getLastName(), usersDto.getLastName());
        assertEquals(user.getEmailId(), usersDto.getEmailId());
        assertEquals(user.getPassword(), usersDto.getPassword());
        assertEquals(user.getIsEnabled(), usersDto.getIsEnabled());
        assertEquals(Role.USER,usersDto.getRole());
    }
}