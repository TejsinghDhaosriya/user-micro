CREATE TABLE users
(
    user_id    int NOT NULL PRIMARY KEY,
    email_id   varchar(255) NOT NULL,
    first_name varchar(255),
    is_enabled int,
    last_name  varchar(255),
    password   varchar(255) NOT NULL,
    role       varchar(255) NOT NULL
);
CREATE SEQUENCE users_sequence
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1 CACHE 20;