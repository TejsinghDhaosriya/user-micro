CREATE TABLE confirmation
(
    token_id           int    NOT NULL PRIMARY KEY,
    confirmation_token varchar(255) NOT NULL,
    created_date       timestamp    NOT NULL,
    user_id            bigint
);

CREATE SEQUENCE confirmation_sequence
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1 CACHE 20;