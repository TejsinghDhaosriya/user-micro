package com.authenticationservice.authenticationservice.config;

import com.authenticationservice.authenticationservice.domain.CustomMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import static com.authenticationservice.authenticationservice.config.MQConfig.EXCHANGE;
import static com.authenticationservice.authenticationservice.config.MQConfig.ROUTING_KEY;

@Service
public class MessagePublisher {
    private final RabbitTemplate template;
    public MessagePublisher(RabbitTemplate template) {
        this.template = template;
    }
    public void publishMessage(@RequestBody CustomMessage message) {
        template.convertAndSend(EXCHANGE,
                ROUTING_KEY, message);
    }
}
