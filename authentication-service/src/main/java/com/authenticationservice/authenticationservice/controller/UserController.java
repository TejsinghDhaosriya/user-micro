package com.authenticationservice.authenticationservice.controller;


import com.authenticationservice.authenticationservice.domain.CustomMessage;
import com.authenticationservice.authenticationservice.config.MessagePublisher;
import com.authenticationservice.authenticationservice.config.TokenUtil;
import com.authenticationservice.authenticationservice.domain.ConfirmationDto;
import com.authenticationservice.authenticationservice.domain.Users;
import com.authenticationservice.authenticationservice.domain.UsersDto;
import com.authenticationservice.authenticationservice.repository.TokenRepository;
import com.authenticationservice.authenticationservice.repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {

    private final UserRepository userRepository;

    private final TokenRepository tokenRepository;

    private final PasswordEncoder bcryptEncoder;

    private final TokenUtil tokenUtil;

    private final UserDetailsService userDetailsService;

    private final MessagePublisher messagePublisher;

    public UserController(UserRepository userRepository, TokenRepository tokenRepository, PasswordEncoder bcryptEncoder, TokenUtil tokenUtil, UserDetailsService userDetailsService, MessagePublisher messagePublisher) {
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
        this.bcryptEncoder = bcryptEncoder;
        this.tokenUtil = tokenUtil;
        this.userDetailsService = userDetailsService;
        this.messagePublisher = messagePublisher;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody Users user) {
        UsersDto existingUser = userRepository.findByEmailIdIgnoreCase(user.getEmailId());
        if (existingUser != null)
            return ResponseEntity.badRequest().body("User already Exists for this email");
        user.setPassword(bcryptEncoder.encode(user.getPassword()));
        UsersDto userDto = new UsersDto(user);
        userRepository.save(userDto);
        ConfirmationDto confirmation = new ConfirmationDto(userDto);
        tokenRepository.save(confirmation);
        messagePublisher.publishMessage(new CustomMessage(user.getEmailId(), confirmation.getConfirmationToken(), "User Registered Successfully!"));
        return ResponseEntity.ok().body("Created");
    }


    @RequestMapping(value = "/verify", method = RequestMethod.GET)
    public ResponseEntity<?> verifyUser(@RequestParam("token") String confirmationToken) {
        ConfirmationDto token = tokenRepository.findByConfirmationToken(confirmationToken);
        if (token != null) {
            UsersDto user = userRepository.findByEmailIdIgnoreCase(token.getUser().getEmailId());
            user.setIsEnabled(1);
            userRepository.save(user);
            messagePublisher.publishMessage(new CustomMessage(user.getEmailId(), null, "User Verified Successfully!"));
            return ResponseEntity.ok().body("User Verified Successfully!");
        } else
            return ResponseEntity.badRequest().body("Verification Failed!");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody Users request) {
        UsersDto user = userRepository.findByEmailIdIgnoreCase(request.getEmailId());
        if (user != null) {
            if (user.getIsEnabled() == 1) {
                if (bcryptEncoder.matches(request.getPassword(), user.getPassword())) {
                    final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmailId());
                    return ResponseEntity.ok().body(tokenUtil.generateToken(userDetails));
                } else
                    return ResponseEntity.badRequest().body("Invalid Password!");
            } else
                return ResponseEntity.badRequest().body("User is Not Verified!");
        } else {
            return ResponseEntity.badRequest().body("User Not Exists!");
        }

    }
}
