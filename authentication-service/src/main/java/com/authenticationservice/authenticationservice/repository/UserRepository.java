package com.authenticationservice.authenticationservice.repository;

import com.authenticationservice.authenticationservice.domain.UsersDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UsersDto, String> {

    UsersDto findByEmailIdIgnoreCase(String emailId);
}
