package com.authenticationservice.authenticationservice.repository;

import com.authenticationservice.authenticationservice.domain.ConfirmationDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends CrudRepository<ConfirmationDto, String> {

    ConfirmationDto findByConfirmationToken(String confirmationToken);


}
