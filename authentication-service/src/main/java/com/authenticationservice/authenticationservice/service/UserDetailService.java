package com.authenticationservice.authenticationservice.service;

import com.authenticationservice.authenticationservice.domain.UsersDto;
import com.authenticationservice.authenticationservice.repository.UserRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDetailService implements UserDetailsService {


    private final UserRepository userDao;

    public UserDetailService(UserRepository userDao) {
        this.userDao = userDao;
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UsersDto user = userDao.findByEmailIdIgnoreCase(email);
        return new User(user.getEmailId(), user.getPassword(), new ArrayList<>());
    }

}
