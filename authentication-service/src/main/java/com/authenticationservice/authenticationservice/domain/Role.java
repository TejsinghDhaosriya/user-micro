package com.authenticationservice.authenticationservice.domain;

public enum Role {
    USER, ADMIN
}
