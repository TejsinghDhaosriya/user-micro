package com.authenticationservice.authenticationservice.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UsersDto {
    @Id
    @SequenceGenerator(name="users_sequence",sequenceName = "users_sequence",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "users_sequence")
    @Column(name="user_id")
    private long userid;


    @Column(name="email_id")
    private String emailId;

    @Column(name="password")
    private String password;

    @Column(name="first_name")
    private String firstName;


    @Column(name="last_name")
    private String lastName;

    @Column(name="is_enabled")
    private int isEnabled = 0;

    @Enumerated(EnumType.ORDINAL)
    private Role role;


    public UsersDto(Users user) {
        this.userid = user.getUserid();
        this.emailId = user.getEmailId();
        this.password = user.getPassword();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.isEnabled = user.getIsEnabled();
        this.role = user.getRole()==null?Role.USER:user.getRole();

    }
}
