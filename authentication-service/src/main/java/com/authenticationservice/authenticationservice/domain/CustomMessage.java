package com.authenticationservice.authenticationservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomMessage {

    private String email;
    private Date emailDate;
    private String token;
    private String description;

    public CustomMessage(String email, String confirmationToken,String description) {
        this.email = email;
        this.token=confirmationToken;
        this.emailDate = new Date();
        this.description = description;
    }
}
