package com.authenticationservice.authenticationservice.domain;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Users {
    private long userid;
    private String emailId;
    private String password;
    private String firstName;
    private String lastName;
    private int isEnabled = 0;
    private Role role;

}

