Create three projects using https://start.spring.io

- API Gateway using Spring Cloud Gateway
- Authorization Service
- Email Service

Implement the following APIs

- SignUp
- Login
- Verify signup

## Architecture

Any API is routed through API Gateway and onto the subsequent service. For this assignment, you only need to wire with one instance of the service. However have some ideas around scaling it for future discussions.

On login, API Gateway should first authenticate with Authorization service and then return with a JWT token in response.


### Data Model

User is fundamental entity. Every User belongs to an organization and has an Account. Account has credentials like username and password and also the roles associated to access. Assume there are two roles: ROLE_USER and ROLE_ADMIN. User has attributes like first name and last name. Account is in a disabled state till the verification process is complete.

### Signup flow

On sign up flow, post the entity creation in database, the authorization service should drop an event message for email service to pick up and sent an email. The email logic doesn't have to be implement but the consumer handler should be.

### Verify Signup

Similar to signup flow, the original request goes to authorization service to enable the account and then a message is dropped to sent an account verified email by email service. Once again, there is no need to implement any external email service integration.

### Testing

It is important to flow Test driven development methodology to build this project. Please leverage integration testing using test containers.

### Deployment

Please use Docker and define docker-compose to bring up the required services. 

### Required services

- Database: PostgresSQL
- Messaging: RabbitMQ
- Version management for DB: flywayDB
- Build tool: Maven

Please leverage spring cloud stream to connect to RabbitMQ and Spring data JPA for postgresSQL.






