package com.gateway.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ApiGatewayApplication {
	final static int port = 8081;
     final static String baseUrl = "http://localhost:"+port;

	@Bean
	public RouteLocator Routes(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(p -> p
						.path("/register")
						.uri(baseUrl+"/register"))
				.route(p -> p
						.path("/verify")
						.uri(baseUrl+"/verify"))
				.route(p -> p
						.path("/login")
						.uri(baseUrl+"/login"))
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}

}
